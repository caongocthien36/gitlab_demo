const express = require("express");
const app = express();
app.get("/", (req, res) => {
    res.send({ hello: "world" });

});




app.get("/sum", (req, res) => {
    var NumA = parseInt(req.query.NumA);
    var NumB = parseInt(req.query.NumB);
    var result = NumA + NumB;
    res.send({ sum: result });

});

const PORT = process.env.PORT || 5000;
app.listen(PORT, function() {
    console.log(`App listening on port ${PORT}`);
});
